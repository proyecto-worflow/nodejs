FROM node:21.7.2 

WORKDIR /src

COPY package*.json ./

RUN npm install

RUN npm i -g nodemon

EXPOSE 3000

COPY . .

CMD ["npm", "start"]